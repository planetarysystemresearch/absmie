# Makefile for absMie
# Antti Penttil�, 2017

EXENAME = absMie

COMP ?= gfortran
FOPT ?= -O2 -fcheck=all -ffree-form -std=f2008

# Object recipies

all : main

main : mod
	$(COMP) -o $(EXENAME) $(FOPT) sudiartaMie.o sudiartaMie-main.f

mod : sudiartaMie.o

sudiartaMie.o :
	$(COMP) -c $(FOPT) sudiartaMie.f

clean : 
	rm -rf *.o *.mod $(EXENAME) $(EXENAME).exe
