PROGRAM SudiartaMieMain

  USE SUDIARTAMIE
  
  REAL(KIND=dp) :: x, cr, ci
  COMPLEX(KIND=dp) :: msph, mmed
  INTEGER :: nang, i, cc
  TYPE(mieResult) :: mr
  CHARACTER(64) :: ca
  
  cc = command_argument_count()
  IF(cc < 3 .OR. cc == 4 .OR. cc > 6) THEN
    WRITE(*,*) "Error, must give 3 to 6 command-line arguments"
    WRITE(*,*) "Usage: absMie size_parameter_in_vacuum sphere_complex_ref_index [medium_complex_ref_index] [no_of_angles]"
    WRITE(*,*) "Hint - input complex numbers as 're im'"
    STOP
  END IF
  
  CALL get_command_argument(1, ca)
  READ(ca, *) x
  CALL get_command_argument(2, ca)
  READ(ca, *) cr
  CALL get_command_argument(3, ca)
  READ(ca, *) ci
  msph = CMPLX(cr,ci,dp)
  IF(cc > 3) THEN
    CALL get_command_argument(4, ca)
    READ(ca, *) cr
    CALL get_command_argument(5, ca)
    READ(ca, *) ci
    mmed = CMPLX(cr,ci,dp)
  ELSE
    mmed = (1.0_dp, 0.0_dp)
  END IF
  IF(cc > 5) THEN
    CALL get_command_argument(6, ca)
    READ(ca, *) nang
  ELSE
    nang = 181
  END IF
  
  WRITE(*,*) "Starting absMie, arguments:"
  WRITE(*,*) " size parameter in vacuum = ", x
  WRITE(*,*) " sphere refractive index = ", msph
  WRITE(*,*) " medium refractive index = ", mmed
  WRITE(*,*) " number of angles = ", nang
  WRITE(*,*) ""
  
  mr = absMie(x, msph, mmed, nang)
  
  WRITE(*,*) "Ready"
  WRITE(*,*) "Qext=", mr%Qext, "Qsca=", mr%Qsca, "Qabs=", mr%Qabs, "g=", mr%g
  WRITE(*,*) "S1 and S2"
  DO i=1,nang
    WRITE(*,*) mr%ang(i), mr%s1(i), mr%s2(i)
  END DO
  WRITE(*,*) "Mueller matrix M11, M12, M33, M34"
  DO i=1,nang
    WRITE(*,*) mr%ang(i), mr%mm(i,:)
  END DO


END PROGRAM SudiartaMieMain
