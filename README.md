# absMie, SCATTERING BY SPHERE IN ABSORBING MEDIA #


* Fortran conversion of Mathematica Mie code for sphere in absorbing medium by Sudiarta
* Fortran module for the actual computations, main program for running from command line
* Version 1.0
* Original Mathematica code credits: Created by I Wayan Sudiarta (sudiarta@dal.ca). Dept. of Physics and Atmospheric Science, Dalhousie University, Halifax, NS B3H 3J5

## Compilation ##

* Complies to Fortran 2008 standard
* Otherwise Fortran 90-compatible, but uses iso_fortran_env for defining number precisions, and command line argument functions
* With gfortran, compile with switches -ffree-form -std=f2008

## Contact ##

* Antti Penttil�, Department of Physics, University of Helsinki, Finland
