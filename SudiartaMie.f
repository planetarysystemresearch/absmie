MODULE SUDIARTAMIE
! Converted from absMie for Mathematica:
! ---
! Created by I Wayan Sudiarta (sudiarta@dal.ca)
! Dept. of Physics and Atmospheric Science
! Dalhousie University, Halifax, NS B3H 3J5
! Last Updated: 24 Aug 2005
! Changes:
! 24 Aug 2005: Fixing the asymmetry g.
! ---
! Conversion by Antti Penttilä
! 2016, Department of Physics
! University of Helsinki, Finland
!
! Fortran 2008-standard
! Compiles, at least, with "gfortran -ffree-from -std=f2008"

  USE, INTRINSIC :: iso_fortran_env
  IMPLICIT NONE
  
  INTEGER, PARAMETER :: dp = REAL64
  REAL(KIND=dp), PARAMETER :: degree = 0.0174532925199433_dp
  
  TYPE mieResult
    INTEGER :: nang
    REAL(KIND=dp) :: angres, Qext, Qsca, Qabs, g
    REAL(KIND=dp), DIMENSION(:), ALLOCATABLE :: ang
    REAL(KIND=dp), DIMENSION(:,:), ALLOCATABLE :: mm
    COMPLEX(KIND=dp), DIMENSION(:), ALLOCATABLE :: s1, s2
  END TYPE mieResult
  
  PUBLIC
  
CONTAINS

! INPUT:
!  xo - size parameter in vacuum
!  msph - sphere refractive index (complex)
!  mmed - medium refractive index (complex)
!  nang - number of angles
FUNCTION absMie(xo, msph, mmed, nang) RESULT(mieRes)

  TYPE(mieResult) :: mieRes
  REAL(KIND=dp), INTENT(IN) :: xo
  COMPLEX(KIND=dp), INTENT(IN) :: msph, mmed
  INTEGER, INTENT(IN) :: nang
  INTEGER :: nstop, ymod, nmx, i, j
  REAL(KIND=dp) :: A, xi2, xi2e, ax, ay, theta, xt
  REAL(KIND=dp), DIMENSION(:), ALLOCATABLE :: p, t
  COMPLEX(KIND=dp) :: mr, k, k2, x, y, ct, ct2, ct3, ct4, ct5
  COMPLEX(KIND=dp), DIMENSION(:), ALLOCATABLE :: psi, psid, psi2, psi2d, &
    xi, xid, an, bn, cn, dn
  
  mieRes%nang = nang
  mieRes%angres = 180.0_dp/(nang-1)
  ALLOCATE(mieRes%ang(nang), mieRes%mm(nang,4), mieRes%s1(nang), mieRes%s2(nang))

  mr = msph/mmed
  k = mmed
  k2 = msph
  x = k*xo
  y = k2*xo
  nstop = INT(ABS(x) + 4.0_dp*ABS(x)**0.3333_dp + 2.0_dp)
  ymod = INT(ABS(y))
  nmx = MAX(nstop, ymod) + 15
  IF(AIMAG(x) /= 0) THEN
    xi2 = 2.0_dp * AIMAG(x)
    xi2e = EXP(xi2)
    A = REAL(k,dp)*(xi2e/xi2 + (1.0_dp-xi2e) / xi2**2)
  ELSE
    A = 0.5_dp * REAL(k,dp)
  END IF
  
  ALLOCATE(psi(0:nstop), psid(0:nstop), psi2(0:nstop), psi2d(0:nstop), &
    xi(0:nstop), xid(0:nstop), an(nstop), bn(nstop), cn(nstop), dn(nstop), &
    p(0:nstop), t(0:nstop))
  CALL rbj(x, psi, psid)
  CALL rbj(y, psi2, psi2d)
  CALL rby(x, xi, xid)
  xi(:) = psi(:) + (0.0_dp, 1.0_dp) * xi(:)
  xid(:) = psid(:) + (0.0_dp, 1.0_dp) * xid(:)
  
  DO i=1,nstop
    ct = psi(i)*psi2d(i)
    ct2 = psi2(i)*psid(i)
    ct3 = mr*(psi(i)*xid(i) - xi(i)*psid(i))
    ct4 = (mr*psi2(i)*xid(i) - xi(i)*psi2d(i))
    ct5 = psi2(i)*xid(i) - mr*xi(i)*psi2d(i)
    an(i) = (mr*ct2 - ct) / ct4
    bn(i) = (ct2 - mr*ct)/ct5
    cn(i) = ct3/ct5
    dn(i) = ct3/ct4
  END DO

  ax = ABS(x)
  ay = ABS(y)
  ! Qext
  ct = (0.0_dp, 0.0_dp)
  DO i=1,nstop
    ct = ct + (2*i+1)*(CONJG(psi(i))*psid(i) - &
         psi(i)*CONJG(psid(i)) + bn(i)*CONJG(psid(i))*xi(i) + &
         CONJG(bn(i))*psi(i)*CONJG(xid(i)) - &
         an(i)*CONJG(psi(i))*xid(i) - &
         CONJG(an(i))*CONJG(xi(i))*psid(i))
  END DO
  mieRes%Qext = -AIMAG(CONJG(k)*ct) / (ax**2 * A)
  ! Qsca
  ct = (0.0_dp, 0.0_dp)
  DO i=1,nstop
    ct = ct + (2*i+1)*(-ABS(an(i))**2 * xid(i)*CONJG(xi(i)) + &
         ABS(bn(i))**2 * xi(i)*CONJG(xid(i)))
  END DO
  mieRes%Qsca = -AIMAG(CONJG(k)*ct) / (ax**2 * A)
  ! Qabs
  ct = (0.0_dp, 0.0_dp)
  DO i=1,nstop
    ct = ct + (2*i+1)*(ABS(dn(i))**2 * psi2d(i)*CONJG(psi2(i)) - &
         ABS(cn(i))**2 * psi2(i)*CONJG(psi2d(i)))
  END DO
  mieRes%Qabs = -AIMAG(CONJG(k2)*ct) / (ay**2 * A)
  ! s1 and s2
  DO i=1,nang
    theta = (i-1)*mieRes%angres
    mieRes%ang(i) = theta
    CALL pitau(COS(theta*degree), p, t)
    ct = (0.0_dp, 0.0_dp)
    ct2 = (0.0_dp, 0.0_dp)
    DO j=1,nstop
      xt = (2.0_dp*j+1.0_dp)/(j*(j+1.0_dp))
      ct = ct + xt*(an(j)*p(j) + bn(j)*t(j))
      ct2 = ct2 + xt*(an(j)*t(j) + bn(j)*p(j))
    END DO
    mieRes%s1(i) = ct
    mieRes%s2(i) = ct2
  END DO
  !WRITE(*,*) mieRes%s1
  ! g
  xt = 0.0_dp
  DO i=1,nstop-1
    xt = xt + (i*(i+2.0_dp))/(i+1.0_dp) * REAL(an(i)*CONJG(an(i+1)) + bn(i)*CONJG(bn(i+1)), dp) + &
      (2.0_dp*i+1.0_dp)/(i*(i+1.0_dp)) * REAL(an(i)*CONJG(bn(i)), dp)
  END DO
  mieRes%g = 2 * xt
  xt = 0.0_dp
  DO i=1,nstop
    xt = xt + (2*i+1)*(ABS(an(i))**2 + ABS(bn(i))**2)
  END DO
  mieRes%g = mieRes%g / xt
  ! Mueller matrix
  DO i=1,nang
    mieRes%mm(i,1) = (ABS(mieRes%s1(i))**2 + ABS(mieRes%s2(i))**2)/2
    mieRes%mm(i,2) = (ABS(mieRes%s2(i))**2 - ABS(mieRes%s1(i))**2)/2
    ct = mieRes%s1(i) * CONJG(mieRes%s2(i))
    mieRes%mm(i,3) = REAL(ct, dp)
    mieRes%mm(i,4) = AIMAG(ct)
  END DO

END FUNCTION absMie


! Ricatti-Bessel y
SUBROUTINE rby(z, sby, dsby)
  COMPLEX(KIND=dp), INTENT(IN) :: z
  COMPLEX(KIND=dp), DIMENSION(0:), INTENT(OUT) :: sby, dsby
  INTEGER :: nmax, i
  
  nmax = SIZE(sby)-1
  IF(SIZE(dsby) /= nmax+1) THEN
    WRITE(*,*) "rby array mismatch, stopping"
    STOP
  END IF
  
  sby(0) = -COS(z)/z
  sby(1) = (sby(0) - SIN(z))/z
  DO i=2,nmax
    sby(i) = (2*i-1)*sby(i-1)/z - sby(i-2)
  END DO
  dsby(0) = (SIN(z) + COS(z)/z)/z
  DO i=1,nmax
    dsby(i) = sby(i-1) - (i+1)*sby(i)/z
  END DO
  dsby(:) = sby(:) + z*dsby(:)
  sby(:) = z*sby(:)

END SUBROUTINE rby


! Ricatti-Bessel j
SUBROUTINE rbj(z, sbj, dsbj)
  COMPLEX(KIND=dp), INTENT(IN) :: z
  COMPLEX(KIND=dp), DIMENSION(0:), INTENT(OUT) :: sbj, dsbj
  INTEGER :: nmax, i, niter
  COMPLEX(KIND=dp) :: sa, sb, cs, f0, f1, f
  
  nmax = SIZE(sbj)-1
  IF(SIZE(dsbj) /= nmax+1) THEN
    WRITE(*,*) "rbj array mismatch, stopping"
    STOP
  END IF
  
  niter = INT(nmax + SQRT(100 + ABS(z)))

  sbj(0) = SIN(z)/z
  sbj(1) = (sbj(0) - COS(z))/z
  sa = sbj(0)
  sb = sbj(1)
  f0 = (0.0_dp, 0.0_dp)
  f1 = (1.0E-10_dp, 0.0_dp)
  DO i=niter,0,-1
    f = (2.0_dp*i + 3.0_dp)*f1/z - f0
    IF(i <= nmax) sbj(i)= f
    f0 = f1
    f1 = f
  END DO
  IF(ABS(sa) >= ABS(sb)) THEN
    cs = sa/f
  ELSE
    cs = sb/f0
  END IF
  DO i=0,nmax
    sbj(i) = cs*sbj(i)
  END DO
  dsbj(0) = (COS(z) - SIN(z)/z)/z
  DO i=1,nmax
    dsbj(i) = sbj(i-1) - (i+1.0_dp)*sbj(i)/z
  END DO
  dsbj(:) = sbj(:) + z*dsbj(:)
  sbj(:) = z*sbj(:)

END SUBROUTINE rbj


! PiTau
SUBROUTINE pitau(mu, p, t)
  REAL(KIND=dp), INTENT(IN) :: mu
  REAL(KIND=dp), DIMENSION(0:), INTENT(OUT) :: p, t
  INTEGER :: i, nmax
  
  nmax = SIZE(p)-1
  IF(SIZE(t) /= nmax+1) THEN
    WRITE(*,*) "pitau array mismatch, stopping"
    STOP
  END IF
  
  p(0) = 0.0_dp
  p(1) = 1.0_dp
  DO i=2,nmax
    p(i) = (2*i-1)*mu*p(i-1)/(i-1) - i*p(i-2)/(i-1)
  END DO
  t(0) = 0.0_dp
  DO i=1,nmax
    t(i) = i*mu*p(i) - (i+1)*p(i-1)
  END DO

END SUBROUTINE pitau
  

END MODULE SUDIARTAMIE
